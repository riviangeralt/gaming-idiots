import React, { useEffect } from 'react';
import { CustomForm } from '../control/useForm';
import { FormGroup, Grid, Paper } from '@mui/material'
import TextFieldController from '../control/TextFieldController';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux'
import { isAuthenticated } from '../auth/Auth';
import { fetchIndividualUser } from '../slices/userSlice';
import TextField from '@mui/material/TextField';

const EditForm = (props) => {
    const selectedUser = useSelector(state => state.users.selectedUser)
    const { control, formState: { errors }, reset, handleSubmit, register, setValue, getValues } = useForm();
    const adminId = isAuthenticated().user._id
    const userId = props?.match?.params?.id
    const dispatch = useDispatch()
    // {
    //     defaultValues: {
    //         name: selectedUser?.name
    //     }
    // }

    useEffect(() => {
        dispatch(fetchIndividualUser({ "admin": adminId, "user": userId }))
        reset({ 'name': selectedUser?.name })
        console.log("ssss", selectedUser)
    }, [dispatch, reset]);

    const onSubmit = (data) => {
        console.log(data)
    }
    console.log(selectedUser?.cartItems)
    return (
        <>
            {selectedUser ?
                <Grid component={Paper} sx={{ padding: '1rem' }}>
                    {selectedUser.name}
                    {selectedUser.email}
                    {/* {selectedUser?.cartItems.map(item => item.createdAt)} */}

                    {/* <input name='name' value={selectedUser.name} /> */}
                    <CustomForm onSubmit={handleSubmit(onSubmit)} style={{ width: '100%', margin: '0 auto', textAlign: 'center' }}>
                        <FormGroup style={{ width: '100%', margin: '1rem 0' }}>
                            <TextFieldController
                                control={control}
                                errors={errors}
                                name='name'
                                label='Name'
                                // defaultValue={getValues('name') ? getValues('name') : selectedUser.name}
                                rules={{
                                    required: 'This field is required',
                                }}
                                register={register}
                            />
                        </FormGroup>
                        <FormGroup style={{ width: '100%', margin: '1rem 0' }}>
                            <TextFieldController
                                control={control}
                                errors={errors}
                                name='email'
                                label='Email'
                                defaultValue={getValues('email') ? getValues('email') : selectedUser?.email}
                                rules={{
                                    required: 'This field is required',
                                    validate: (value) =>
                                        /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/.test(
                                            value
                                        ) || "Please enter the valid email id",
                                }}
                                register={register}
                            />
                        </FormGroup>
                    </CustomForm>
                    <Grid item lg={8}></Grid>
                </Grid>
                : 'Loading...'}
        </>
    );
};

export default EditForm;
