const Category = require('../models/categoryModel')
const { errorHandler } = require('../helpers/dbErrorHandler')

exports.create = (req, res) => {
    const category = new Category(req.body)
    category.save((err, data) => {
        if (err) {
            return res.status(400).json({ err: errorHandler(err) })
        }
        res.json({ data })
    })
}